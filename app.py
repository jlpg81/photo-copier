# Open up tkinter window
# User selects copy source
# User selects copy destination
# Copy button unfreezes
# All the files from source appear on the list. The program will say if there are non image files, give a warning with the first 5 files.
# ver pelicula el hoyo/the platform
# Person clicks on the copy button
# For each file:
# Get date metadata
# Copy to the destination folder.
# Rename file in destination. Meta is YYYY-MM-DD
# If there are any errors, report them at the end of the transfer. The number of errors.


##################################################################
import os, datetime, shutil
from tkinter import *
import tkinter.filedialog, tkinter.ttk
from functools import partial

# Define internal functions:
def photocopier(source, destination):
    print("source is "+source)
    file_list = os.listdir(source)
    print(file_list)
    for obj in file_list:
        print("file is: "+str(obj))
        print(os.path.join(source,obj))
        cwf = os.path.join(source,obj)
        if os.path.isfile(cwf):
            print("debug: "+obj+" is a file")
            creation_time = datetime.datetime.utcfromtimestamp(os.path.getctime(cwf)).strftime('%Y-%m-%d_%Hh%M')
            destination_file = destination+'/'+creation_time+'_'+obj
            file_location = os.path.realpath(cwf)
            print("debug: "+file_location)
            shutil.copy2(src=file_location,dst=destination_file)
        elif os.path.isdir(cwf) and recursivebouton.instate(['selected']):
            print("debug: "+obj+" is a folder!")
            new_source = source+'/'+obj
            new_destination = destination+'/'+obj
            try:
                os.mkdir(path=new_destination)
                print("folder created")
            except:
                print("folder not created")
                pass
            print(new_source)
            print(new_destination)
            photocopier(source=new_source,destination=new_destination)
        elif os.path.isdir(cwf):
            pass
        else:
            print("an error ocurred identifying the type of file")
    completion_popup()


# Define tkinter functions
def browse_source():
    global source_folder_path
    filename = tkinter.filedialog.askdirectory()
    source_folder_path.set(filename)

    global original_dir
    original_dir = str(filename)
    print("debug: original_dir: "+original_dir)

    global original_dir_size
    # size = str(os.stat(filename).st_size)+" bytes"
    # size = sum(os.path.getsize(f) for f in os.listdir('.') if os.path.isfile(f))
    # size = sum(os.stat(f).st_size for f in os.listdir(filename))
    size = 0
    start_path = filename  # To get size of current directory
    for path, dirs, files in os.walk(start_path):
        print(files)
        for f in files:
            try:
                fp = os.path.join(path, f)
                size += os.path.getsize(fp)
            except:
                print("an error has occurred calculating size")
    size = int(size/1048576)
    size = str(size)+" Mb"
    original_dir_size.set(size)
    print("debug: size "+str(size))

def browse_destination():
    global destination_folder_path
    filename = tkinter.filedialog.askdirectory()
    destination_folder_path.set(filename)

    global destination_dir
    destination_dir = str(filename)
    print("debug: destination_dir: "+destination_dir)

    global destination_dir_size
    size = str(int(shutil.disk_usage(destination_dir)[2]/1048576))+" Mb"
    destination_dir_size.set(size)
    print("debug: size "+str(os.stat(filename).st_size))

# Completion message
def completion_popup():
    popup = tkinter.Tk()
    completion_message = tkinter.ttk.Label(popup, text = "Program has finished running.\nPlease verify the results, and inform me of any bugs.")
    completion_message.pack( padx=20, pady=20)
    B1 = tkinter.ttk.Button(popup, text="Ok", command=popup.destroy)
    B1.pack()
    popup.mainloop()

# Open up tkinter window
root = Tk()
root.title("Photo-Copier")

instruction_label = LabelFrame(root, text="Instructions", padx=200, pady=20)
instruction_label.grid(row=0, column=1, columnspan=4, padx=20, pady=20)
Label(instruction_label, text="""1) Select source folder
2) Select destination folder.
3) Make sure the destination folder has enough space to copy everything.
4) Check the "Copy subfolders" checkbox if you want to copy subdirectories.
4) Click on 'Copy'. """, justify=LEFT).grid(row=0,column=1, columnspan=3)
photo = PhotoImage(file = r"/home/jorge/dev/photo-copier/copier-logo.png")
photoimage = photo.subsample(1, 1) 
Label(root, image = photoimage, justify=LEFT).grid(row=0, column=0, padx=20, pady=20) 

source_label = Label(root, text="Source Folder: ")
source_label.grid(row=1, column=0)
source_folder_path = StringVar()
source_directory_label = Label(master=root,textvariable=source_folder_path)
source_directory_label.grid(row=1, column=1, padx=100, pady=10)
source_directory_size_label = Label(root, text="Folder weight: ")

source_directory_size_label.grid(row=1, column=2)
original_dir_size = StringVar()
source_directory_size = Label(master=root, textvariable=original_dir_size)
source_directory_size.grid(row=1, column=3)

browse_source_button = Button(text="Browse", command=browse_source)
browse_source_button.grid(row=1, column=4)

destination_label = Label(root, text="Destination Folder: ")
destination_label.grid(row=2, column=0)
destination_folder_path = StringVar()
destination_directory_label = Label(master=root,textvariable=destination_folder_path)
destination_directory_label.grid(row=2, column=1)
destination_directory_size_label = Label(root, text="Free Space: ")

destination_directory_size_label.grid(row=2, column=2)
destination_dir_size = StringVar()
destination_directory_size = Label(master=root, textvariable=destination_dir_size)
destination_directory_size.grid(row=2, column=3)

browse_destination_button = Button(text="Browse", command=browse_destination)
browse_destination_button.grid(row=2, column=4)

recursivebouton = tkinter.ttk.Checkbutton(root, text="Copy subfolders")
recursivebouton.grid(row=3, columnspan=5, padx=0, pady=10)

copybutton = Button(root, text="Copy", command=lambda: photocopier(original_dir,destination_dir))
copybutton.grid(row=4, columnspan=5, padx=0, pady=10)

root.tk.call('wm', 'iconphoto', root._w, tkinter.PhotoImage(file=r"/home/jorge/dev/photo-copier/copier-logo.png"))
root.mainloop()


# All the files from source appear on the list.


# The program will say if there are non image files, give a warning with the first 5 files.

# ver pelicula el hoyo/the platform
# Copy button unfreezes
# Person clicks on the copy button

# For each file:


# If there are any errors, report them at the end of the transfer. The number of errors.